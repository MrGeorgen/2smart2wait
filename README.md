# 2smart2wait
This repo moved to my own [website](https://git.redstoneunion.de/MrGeorgen/2smart2wait)<br>
A fork of the popular proxy to wait out 2b2t.org's way too long queue.(https://github.com/surprisejedi/2lazy2wait)

# Features
- costum ETA calculator
- use the play command if you to play at a specified time
- chunk caching
- stores the minecraft login token to avoid account blocking

# How to install
1. Download node.js and install it. On non-windows platforms, you also need npm.
2. Download this repository with the green button (top right of this page). If you downloaded it as zip, unzip it.
3. Open a terminal and navigate to the folder you downloaded it
4. Run `npm install`
5. copy secrets.json.example and name it secrets.json. Fill out your minecraft information in the file. Note that you must use your email adress and not your minecraft username.
6. If you so wish, edit the configuration in config.json. (On Linux ports below 1024, including port 80, require you to run the program with administrator rights.)
7. You need to create a discord bot. Search for a tutorial if you do not how to do that.
8. Edit the file secrets.json and replace BOT_TOKEN with your bots token
9. For trust reasons, this tool does not update automatically. Check back here once in a while to see if there are any updates.

# How to use
1. Read the code to ensure i'm not stealing your credentials. i'm not, but you shouldn't take my word for it. If you don't know how to read it, downloading stuff off the internet and giving it your password is probably a bad idea anyway.
2. Run 'npm start'
3. Your bot should be online now, in discord it should show up with "Queue stopped."
4. See below for commands on how to start the queue.
5. You can access the original 2bored2wait web interface from http://localhost
6. Once the queue reaches a low number, connect to the minecraft server at address `localhost`.
7. After you log off, click the "stop queuing" button. This is really important, as you will not actually disconnect from 2b2t until you do that.

# Commands
All commands can be used through discord or the cli.
- 'start' will start the queue. It takes between 15-30 seconds for the bot to update with the queue position.
- 'start 14:00' will start at 2pm.
- 'play 8:00' will try to calculate the right time to join so you can at 8:00
- 'update' will send an update to the current channel with your position and ETA.
- 'stop' will stop the queue.

# Forewarning
Do not give your secrets.json file to anyone under any circumstances.

Do not repeatedly stop and start the queue, eventually you will not be able to log in.

# Known issues
- starting the queue will revoke your minecraft token. this means that you will not be able to join normal minecraft servers until you restart the game
- starting the queue too many times in a row can sometimes boot you out of your minecraft account (starting the queue or connecting in the minecraft client will tell you "wrong email or password"). to fix this, log in to your account at minecraft.net, then restart minecraft. both of these issues are limitations put in place by mojang to prevent account stealing, and are not fixable.
- Some people report not being able to ride animals using this proxy.
- 2b2t sometimes bugs out and removes you from the queue without telling you. In this case, your queue position will no longer move. Reconnect to fix this.
- If you connect after the the queue is finnished or reconnect the proxy will send cached chunk data. Otherwise you would fly in a emtpy world. Other data however like entities are not chached and will not displayed correctly. You can move out of render distance and come back to fix this issue. Sometimes the client renders a cached chunk with a blank texture.
